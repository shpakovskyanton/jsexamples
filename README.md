# JS examples

## /patterns

### facade.js
Facade allows us to create an object that provides a common interface for working with subsystems. The implementation of the subsystems is hidden from the user, and the work with the components of the subsystem is carried out through the facade.

### adapter.js
The pattern is intended to use the functions of an object that cannot be modified through the creation of an interface. It allows objects to interact with different interfaces without changing the objects themselves.

### mediator.js
Mediator allows you to interact with the system through an intermediary, excluding direct interaction. Mediator is useful in increasing the complexity of systems, as it helps to weaken the connections of systems.

### module.js
Module is immediately invoked function expression which allows us to group and isolate data using closures.

### singleton.js
Singleton class returns one instance.

### factory.js
Factory creates instances. Factory can automatically initialize instances on creation and hide initialization.

### decorator.js
Decorators allows us to add new properties to objects.

### observer.js
Observer class subscribes callbacks to event and calls it when event occurs.

### strategy.js
Strategy allows us to change class behavior.

### prototype.js
Prototype allows you to create object copies. The object itself to responsible clone.

### builder.js
A builder is a pattern that allows you to create complex objects step by step. It allows you to use the same code to get objects with different properties.