'use strict';

/**
 * Target
 * Default file search engine. Uses without adapter.
 */
class DefaultFileSearchEngine {
    /**
     * Constructor
     */
    constructor() {
        /* ... */
    }

    /**
     * Search for text in file
     * @param file file name
     * @param encoding text encoding
     * @param text text for search
     */
    textSearch(file, encoding, text) {
        /* ... */
    }
}

/**
 * Adaptee
 * Advanced search engine. We can create adapter to use AdvancedFileSearchEngine with Search
 */
class AdvancedFileSearchEngine {

    /**
     * Constructor
     */
    constructor() {
        /* ... */
    }

    /**
     * Sets file name
     * @param file
     */
    setFile(file) {
        /* ... */
    }

    /**
     * Sets file encoding
     * @param encoding
     */
    setEncoding(encoding) {
        /* ... */
    }

    /**
     * Sets search algorithm
     * @param algorithm
     */
    setSearchAlgorithm(algorithm) {
        /* ... */
    }

    /**
     * Searches for text
     * @param text
     */
    search(text) {
        /* ... */
    }
}

/**
 * Client
 * Uses search engine for search text in file
 */
class Search {
    /**
     * Constructor
     * @param file file name
     * @param encoding text encoding
     * @param text text for search
     * @param engine search engine
     */
    constructor(file, encoding, text, engine) {
        this.file = file;
        this.encoding = encoding;
        this.text = text;
        this.engine = new engine();
    }

    /**
     * Searches for text in file
     */
    search() {
        this.engine.textSearch(this.file, this.encoding, this.text);
    }
}

/**
 * Adapter uses inheritance from adaptee class. Implements interface.
 */
class InheritanceAdapter extends AdvancedFileSearchEngine {
    /**
     * Calls super constructor and sets aho-corasick as algorithm.
     */
    constructor() {
        super();
        this.setSearchAlgorithm('aho-corasick');
    }

    /**
     * Implements textSearch method
     * @param file file name
     * @param encoding file encoding
     * @param text text for search
     * @returns {*}
     */
    textSearch(file, encoding, text) {
        this.setFile(file);
        this.setEncoding(encoding);
        return this.search(text);
    }
}

/**
 * Adapter uses composition. Implements interface.
 */
class CompositionAdapter {
    /**
     * Creates this.engine
     */
    constructor() {
        this.engine = new AdvancedFileSearchEngine()
    }

    /**
     * Implements textSearch method
     * @param file file name
     * @param encoding file encoding
     * @param text text for search
     */
    textSearch(file, encoding, text) {
        this.engine.setFile(file);
        this.engine.setEncoding(encoding);
        this.engine.setSearchAlgorithm('aho-corasick');
        return this.engine.search(text);
    }
}

// Use default engine
const defaultFileSearch = new Search('file.txt', 'utf-8', 'pattern', DefaultFileSearchEngine);
defaultFileSearch.search();

// Use engine with InheritanceAdapter
const advancedInheritanceFileSearch = new Search('file.txt', 'utf-8', 'pattern', InheritanceAdapter);
advancedInheritanceFileSearch.search();

// Use engine with CompositionAdapter
const advancedCompositionFileSearch = new Search('file.txt', 'utf-8', 'pattern', CompositionAdapter);
advancedCompositionFileSearch.search();