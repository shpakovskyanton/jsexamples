'use strict';

/*
 * Singleton class returns one instance
 */
function Singleton() {
    // If there is no instance then save this instance
    if (!Singleton.hasOwnProperty('instance')) {
        Singleton.instance = this;
    }
    return Singleton.instance;
}
// Test out class
console.log(new Singleton() === new Singleton());