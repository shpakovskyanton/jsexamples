'use strict';

/*
    Different classes with different constructors
 */
class Car {
    /* ... */
}

class Aircraft {
    /* ... */
}

class Train {
    /* ... */
}

class Bus {
    /* ... */
}

/**
 * Factory creates objects depends on type. Factory allows you hide objects initialization from user.
 */
class Factory {
    /**
     * Create concrete instance. Concrete classes constructors can use different arguments sets.
     * @param type
     * @returns {*}
     */
    create(type) {
        if (type === 'car') return new Car();
        if (type === 'aircraft') return new Aircraft();
        if (type === 'train') return new Train();
        if (type === 'bus') return new Bus();
        return null;
    }
}

// Create factory
const factory = new Factory();
// Create car instance
const car = factory.create('car');
// Create bus instance
const bus = factory.create('bus');


