'use strict';


/**
 * Box is a container that contains objects with common interface
 */
class Box {

    constructor() {
        this.items = [];
    }

    push(item) {
        this.items.push(item);
        return this;
    }

    weight() {
        return this.items.reduce((result, item) => {
            return result += item.weight()
        }, 0);
    }
}

/**
 * Cat class
 */
class Cat {
    constructor(name = 'unnamed', weight = 0) {
        this._name = name;
        this._weight = weight;
    }

    weight() {
        return this._weight;
    }
}

// box with other boxes and cats inside
const box = new Box();
box.push(new Cat('pirat', 4))
    .push(new Cat('barsik', 5))
    .push(new Cat('ugly', 3))
    .push(new Box()
        .push(new Cat('bob', 1))
        .push(new Cat('t-rex', 5.2))
    );
console.log(box.weight());
