"use strict";

/**
 * Abstract command class
 */
class Command {
    constructor(users) {
        this.users = users !== undefined ? users : [];
    }

    execute() {
        // There is no realization of this method
    }
}

/**
 * Remove user from list
 */
class RemoveUserCommand extends Command {
    execute(user) {
        this.users = this.users.filter(item => item !== user);
    }
}

/**
 * Add user to list
 */
class AddUserCommand extends Command {
    execute(user) {
        if (!this.users.contains(user)) this.users.push(user)
    }
}
