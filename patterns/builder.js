'use strict';

/*
A builder is a pattern that allows you to create complex objects step by step. It allows you to use the same code to get objects with different properties.
 */

/**
 * Archer class with constructor initializer and setters
 */
class Archer {
    constructor(hp = 100, strength = 50, speed = 80, accuracy = 60, evasion = 90, endurance = 60, enhances = []) {
        this._hp = hp;
        this._strength = strength;
        this._speed = speed;
        this._accuracy = accuracy;
        this._evasion = evasion;
        this._endurance = endurance;
        this._enhances = enhances;
    }

    set hp(value) {
        this._hp = value;
    }

    set strength(value) {
        this._strength = value;
    }

    set speed(value) {
        this._speed = value;
    }

    set accuracy(value) {
        this._accuracy = value;
    }

    set evasion(value) {
        this._evasion = value;
    }

    set endurance(value) {
        this._endurance = value;
    }

    addEnhance(enhance) {
        this._enhances.push(enhance);
    }

}

/**
 * Unit enhancing
 */
class Enhance {
}

/**
 * Simple archer builder.
 */
class SimpleArcherBuilder {
    constructor() {
        return new Archer(100, 50, 80, 60, 90, 80);
    }
}

/**
 * Enhanced archer with custom
 */
class EnhancedArcherBuilder {
    constructor(enhance) {
        const archer = new Archer();
        archer.addEnhance(enhance);
        return archer;
    }
}

/**
 * Long bow archer
 */
class LongBowArcherBuilder {
    constructor() {
        const archer = new Archer();
        archer.strength = 90;
        archer.speed = 40;
        archer.evasion = 50;
        archer.accuracy = 90;
        archer.endurance = 60;
        return archer;
    }
}

/*
 Create archers using builders
 */
const archer = new SimpleArcherBuilder();
const enchancedArcher = new EnhancedArcherBuilder(new Enhance());
const longbowArcher = new LongBowArcherBuilder();