'use strict';


/**
 * Android button
 */
class AndroidButton {
    click() {
    }

    setText(text) {
    }
}

/**
 * iOS button
 */
class IOSButton {
    click() {
    }

    setText(Text) {
    }
}

/**
 * Buttons factory creates buttons depends on OS type
 */
class ButtonsFactory {
    constructor(type) {
        if (type === 'android') {
            this.button = AndroidButton;
        } else if (type === 'ios') {
            this.button = IOSButton;
        }
    }

    /**
     * Creates button instance
     * @returns {AndroidButton|IOSButton}
     */
    createButton() {
        return new this.button();
    }
}

// factory and method call
const factory = ButtonsFactory('ios');
factory.createButton();