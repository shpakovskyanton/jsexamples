'use strict';

/*
    Mediator allows you to interact with the system through an intermediary, excluding direct interaction.
    Mediator is useful in increasing the complexity of systems, as it helps to weaken the connections of systems.
    Events monitor is mediator class. It allows us to subscribe to events, emit events ans register participants which
    can use "subscribe" and "publish" methods of the class.
 */
class EventsMonitor {
    constructor() {
        /*
         Subscribers lists. Each event contains subscribers list
        {
            "event1": {
                "subscribers": []
            },
            ...
        }
        */
        this.events = {};
    }

    /**
     * Subscribe callback to event
     * @param event event name
     * @param callback
     */
    subscribe(event, callback) {
        if (!this.events.hasOwnProperty(event)) {
            this.events[event] = {subscribers: []};
        }
        this.events[event].subscribers.push(callback);
    }

    /**
     * Publish event and value
     * @param event event name
     * @param value
     */
    publish(event, value) {
        if (this.events.hasOwnProperty(event) && this.events[event].hasOwnProperty('subscribers')) {
            this.events[event].subscribers.forEach(callback => callback(value));
        }
    }

    /**
     * Register new participant
     * @param participant
     */
    register(participant) {
        participant.events = this.events;
        participant.subscribe = this.subscribe;
        participant.publish = this.publish;
    }
}

// UI components
const button1 = {name: 'button1'};
const button2 = {name: 'button2'};
const textField = {};
// Events monitor
const eventsMonitor = new EventsMonitor();
// Registering components
eventsMonitor.register(button1);
eventsMonitor.register(button2);
eventsMonitor.register(textField);
// Subscribe callback functions
eventsMonitor.subscribe('click', (button) => {
    console.log('Clicked ' + button);
});
eventsMonitor.subscribe('text-changed', (text) => {
    console.log('Text changed to ' + text);
});
// Emit events
button1.publish('click', button1.name);
button2.publish('click', button2.name);
textField.publish('click', 'mediator is awesome');
