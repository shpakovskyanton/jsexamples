'use strict';

/* Default pizza with cheese and pepper */
class Pizza {

    constructor() {
        this.diameter = 30;
        this.weight = 500;
        this.ingredients = ['cheese', 'dough', 'pepper'];
    }
}

/*
    Decorators allows us to add new properties to objects.
 */
/* Hot pizza with chili */
class PizzaWithChili {

    constructor(pizza) {
        this.diameter = pizza.diameter;
        this.weight = pizza.weight + 10;
        this.ingredients = pizza.ingredients.concat(['chili']);
    }
}

/* Pizza with bacon */
class PizzaWithBacon {

    constructor(pizza) {
        this.diameter = pizza.diameter;
        this.weight = pizza.weight + 30;
        this.ingredients = pizza.ingredients.concat(['bacon']);
    }
}

/* Pizza with olives */
class PizzaWithOlives {

    constructor(pizza) {
        this.diameter = pizza.diameter;
        this.weight = pizza.weight + 20;
        this.ingredients = pizza.ingredients.concat(['olives']);
    }
}

// Create pizza
const pizza = new PizzaWithBacon(new PizzaWithChili(new PizzaWithOlives(new Pizza())));
// Print pizza ingredients
console.log('Ingredients: ' + pizza.ingredients.join(', '));
console.log('Diameter:    ' + pizza.diameter);
console.log('Weight:      ' + pizza.weight);
