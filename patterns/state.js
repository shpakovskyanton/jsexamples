"use strict";

/**
 * Assassin class. Allows to control game unit.
 */
class Assassin {
    constructor(name) {
        this.name = name;
        this.hp = 1;
        this.position = 0;
        this._state = new State(this);
    }

    set state(newState) {
        this._state = newState;
    }

    get state() {
        return this._state;
    }

    attack(enemy) {
        this._state.attack(enemy);
    }

    defence() {
        this._state.defence();
    }

    hide() {
        this._state.hide();
    }

    move(speed) {
        this._state.move(speed);
    }

    damage(value) {
        this.hp -= value;
    }

    show() {
        console.log(`${this.name}\tHP: ${this.hp}, Position: ${this.position}`);
    }
}

/**
 * State prototype
 */
class State {
    constructor(unit) {
        this.unit = unit;
    }

    attack(enemy) {
        this.unit.state = new AttackState(this.unit);
    }

    defence() {
        this.unit.hp *= 2;
        this.unit.state = new DefenceState(this.unit);
    }

    hide() {
        this.unit.state = new HideState(this.unit);
    }

    move(speed) {
        this.unit.state = new MoveState(this.unit);
    }
}

/**
 * Move state. Allows to change assassin position
 */
class MoveState extends State {
    move(speed) {
        this.unit.position += speed;
    }
}

/**
 * Attack state allows to attack the enemy and doesn't allow to change state to hide.
 */
class AttackState extends State {
    attack(enemy) {
        enemy.damage(0.1);
    }

    hide() {
        console.log("Can not hide while attacking");
    }

}

/**
 * Defence state increases assassin HP value
 */
class DefenceState extends State {

    attack(enemy) {
        this.unit.hp /= 2;
        this.unit.state = new AttackState(this.unit);
    }

    hide() {
        this.unit.hp /= 2;
        this.unit.state = new HideState(this.unit);
    }

    move(speed) {
        this.unit.hp /= 2;
        this.unit.state = new MoveState(this.unit);
    }

    defence() {
    }
}

/**
 * Hide state. Assassin can't move attack and defence in this state
 */
class HideState extends State {
}

// Create two fighters and... Let the battle begin!
const white = new Assassin("white");
const black = new Assassin("black");
console.log("Initial state");
white.show(); black.show();

white.attack();
white.attack(black);
console.log("White attacked black");
white.show(); black.show();

white.defence();
black.attack();
black.attack(white);
console.log("White defend, black attacked white");
white.show(); black.show();

black.move();
black.move(1);
white.move();
white.move(2);
console.log("White and black moved");
white.show(); black.show();

