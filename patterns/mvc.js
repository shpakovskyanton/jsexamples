'use strict';

/**
 * Model provides data and methods to work with data (database queries, data checks and etc.).
 * When data changes model notifies subscribers but model don't depends on view. Different views can use one model.
 */
class Model {
    /**
     * Constructor
     */
    constructor() {
        this.vals = {};
        this.subsrcibers = [];
    }

    /**
     * Data manipulation method
     * @param key
     * @param value
     */
    setValue(key, value) {
        this.vals[key] = value;
        this.subsrcibers.forEach(subscriber => {
            subscriber(key, value);
        });
    }

    /**
     * Subscribe on data changing
     * @param subscriber
     */
    onValueChanged(subscriber) {
        if (this.subsrcibers.includes(subscriber) === false)
            this.subsrcibers.push(subscriber);
    }
}

/**
 * Controller creates links between user and system. Controller receives data from user to system.
 */
class Controller {
    /**
     * Constructor
     * @param model
     */
    constructor(model) {
        this.model = model;
    }

    /**
     * Data modification method. It sends data from user to model.
     * @param key
     * @param value
     */
    setValue(key, value) {
        this.model.setValue(key, value);
    }

}

/**
 * View takes data from model and sends it to user.
 */
class View {
    /**
     * Constructor
     * @param name
     * @param model
     */
    constructor(name, model) {
        this.name = name;
        this.model = model;
        // Subscribe callback function
        this.model.onValueChanged((key, value) => {
            let data = {};
            data[key] = value;
            this.display(data);
        });
        this.display(this.model.vals);
    }

    /**
     * Displays changed data
     * @param data
     */
    display(data) {
        console.log(this.name);
        Object.keys(data).forEach((key) => {
            console.log(key + ' => ' + data[key]);
        });
        console.log('\n');
    }
}

/*
   Create Model, View and Controller and set values in the model by controller. Two different views creates.
   View will dynamically display changed values.
 */
const model = new Model();
const controller = new Controller(model);
controller.setValue('gender', 'male');
const view1 = new View('view1', model);
const view2 = new View('view2', model);
controller.setValue('name', 'Bill');
controller.setValue('age', '20');
controller.setValue('citizen', 'true');
