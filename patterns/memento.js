"use strict";

/**
 * Snapshot object. Allows to store and get the state.
 */
class Snapshot {
    constructor(text, file) {
        this._text = text;
        this._file = file;
    }

    get text() {
        return this._text;
    }

    get file() {
        return this._file;
    }
}

/**
 * Manager that allows us to create new snapshot or restore previous editor's state
 */
class SnapshotManager {
    constructor(editor) {
        this._snapshots = [];
        this._editor = editor;
    };

    // Create snapshot
    create() {
        this._snapshots.push(this._editor.snapshot);
    }

    // Restore previous snapshot
    restore() {
        if (this._snapshots.length > 0)
            this._editor.snapshot = this._snapshots.pop();
    }
}

/**
 * Text editor which has text and name of editable file.
 */
class Editor {

    constructor(text, file) {
        this._text = text;
        this._file = file;
    }

    get text() {
        return this._text;
    }

    set text(value) {
        this._text = value;
    }

    get file() {
        return this._file;
    }

    set file(value) {
        this._file = value;
    }

    // Create new snapshot of current editor state
    get snapshot() {
        return new Snapshot(this._text, this._file);
    }

    // Restore editor state from snapshot
    set snapshot(snapshot) {
        this._text = snapshot.text;
        this._file = snapshot.file;
    }

}

// Create editor and manager
const editor = new Editor("hello world", "greeting.txt");
const manager = new SnapshotManager(editor);
// create snapshot
manager.create();
// change text and create snapshot again
editor.text = "alice";
manager.create();
// change file and create snapshot again
editor.file = "alice.dat";
manager.create();
// Clear editor
editor.file = "";
editor.text = "";
console.log("Final state:");
console.log(JSON.stringify(editor));
// restore previous state
console.log("Previous state:");
manager.restore();
console.log(JSON.stringify(editor));
// again
console.log("Previous state:");
manager.restore();
console.log(JSON.stringify(editor));
// and restore it again
console.log("Previous state:");
manager.restore();
console.log(JSON.stringify(editor));