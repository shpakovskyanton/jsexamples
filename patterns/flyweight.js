'use strict';

/**
 * Asteroid class
 */
class Asteroid {

    constructor(diameter, mass, color) {
        this.diameter = diameter;
        this.mass = mass;
        this.color = color;
    }

}

/**
 * Asteroids factory
 */
class AsteroidFactory {
    constructor(asteroid) {
        this.asteroid = asteroid;
    }

    get instance() {
        return this.asteroid;
    }
}

/**
 * Draws defined count of asteroids
 * @param factory Asteroids factory
 * @param count asteroids count
 */
function drawAsteroidsBelt(factory, count) {
    while (count-- > 0) {
        console.log(JSON.stringify(factory.instance) + ' at ' + JSON.stringify({x: Math.random(), y: Math.random()}));
    }
}

const smallIceAsteroidFactory = new AsteroidFactory(new Asteroid(1.0, 9.0, 'white'));
const mediumPinkAsteroidFactory = new AsteroidFactory(new Asteroid(5.0, 4.0, 'pink'));
const bigRedAsteroidFactory = new AsteroidFactory(new Asteroid(20.0, 10.0, 'red'));

drawAsteroidsBelt(smallIceAsteroidFactory, 10);
drawAsteroidsBelt(mediumPinkAsteroidFactory, 2);
drawAsteroidsBelt(bigRedAsteroidFactory, 9);
