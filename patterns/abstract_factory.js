'use strict';

/**
 * Android button
 */
class AndroidButton {
    click() {
    }

    setText(text) {
    }
}

/**
 * Android text
 */
class AndroidText {
    setText(text) {
    }
}

/**
 * iOS button
 */
class IOSButton {
    click() {
    }

    setText(Text) {
    }
}

/**
 * iOS text
 */
class IOSText {
    setText(text) {
    }
}

/**
 * Android factory
 */
class AndroidFactory {
    createButton() {
        return new AndroidButton();
    }

    createText() {
        return new AndroidText();
    }
}

/**
 * iOS factory
 */
class IOSFactory {
    createButton() {
        return new IOSButton();
    }

    createText() {
        return new IOSText();
    }
}

/**
 * UI builder
 */
class UI {
    constructor(UIFactory) {
        this.button = UIFactory.createButton();
        this.text = UIFactory.createText();
    }
}

// factory instance
let factory;
// select factory class
if (OS === 'ios') {
    factory = new IOSFactory();
} else if (OS === 'android') {
    factory = new AndroidFactory();
}
// build UI for concrete OS
const ui = new UI(factory);