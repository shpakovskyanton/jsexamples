'use strict';

// Strategies
function multiply() {
    return this.x * this.y;
}

function divide() {
    return this.x / this.y;
}

/*
    Calculator uses strategy to do an action
 */
class Calculator {

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    setStrategy(strategy) {
        this.strategy = strategy;
    }

    execute() {
        return this.strategy.apply(this);
    }
}

// Use different strategies without any Calculator class changes
const calculator = new Calculator(100, 10);
calculator.setStrategy(multiply);
console.log(calculator.execute());
calculator.setStrategy(divide);
console.log(calculator.execute());