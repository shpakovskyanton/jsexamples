'use strict';

/**
 * Image class
 */
class Image {
    /**
     * Loads image from url immediately
     * @param url
     */
    getImageURL(url) {
    };
}

/**
 * Cached image is the proxy
 */
class CachedImage {
    constructor() {
        this.images = {};
        this.image = new Image();
    }

    /**
     * Loads image from url if image is not in the cache
     * @param url
     */
    getImageURL(url) {
        if (!this.images.hasOwnProperty(url)) {
            this.images[url] = this.image.getImageURL(url);
        }
        return this.images[url];
    };
}

const image = new CachedImage();
image.getImageURL('cat.bmp');
image.getImageURL('cat.bmp');
image.getImageURL('dog.bmp');