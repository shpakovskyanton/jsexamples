'use strict';

/**
 Basic button class
 */
class Button {
}

/**
 * Android button class. Draws android button.
 */
class AndroidButton extends Button {
    draw() {
        console.log('Android button');
    }
}

/**
 * iOS button class. Draws iOS button.
 */
class IOSButton extends Button {
    draw() {
        console.log('iOS button');
    }

}

/**
 * UI delegates button to draw
 */
class UI {
    constructor(button) {
        this.button = button;
    }

    drawButton() {
        this.button.draw();
    }
}

/**
 * RedUI extends UI class without button modification.
 */
class RedUI extends UI {
    drawRed() {
        console.log('Red color button');
    }
}

/*
 Create different buttons
 */

const andrroidUI = new UI(new AndroidButton());
const iOSUI = new UI(new IOSButton());
const redAndroidUI = new RedUI(new AndroidButton());

andrroidUI.drawButton();
iOSUI.drawButton();
redAndroidUI.drawButton();
redAndroidUI.drawRed();