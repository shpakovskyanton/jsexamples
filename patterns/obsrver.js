'use strict';

/**
 * Observable subscribes observers and notifies when events emits
 */
class Observable {

    constructor() {
        this.events = {};
    }

    /**
     * Subscribe callback function to event
     * @param event
     * @param callback
     */
    on(event, callback) {
        if (!this.events.hasOwnProperty(event)) {
            this.events[event] = {observers: []};
        }
        this.events[event].observers.push(callback);
    }

    /**
     * Emit event and call observers
     * @param event
     * @param value
     */
    emit(event, value) {
        if (this.events.hasOwnProperty(event)) {
            this.events[event].observers.forEach(observer => observer(value));
        }
    }
}

// Create observable
const observable = new Observable();
// Subscribe callbacks
observable.on('message', (message) => {
    console.log('Observer1: ' + message);
});
observable.on('message', (message) => {
    console.log('Observer2: ' + message);
});
// Emit event
observable.emit('message', 'hi');
