'use strict';

/*
Module is immediately invoked function expression which allows us to group and isolate data using closures.
 */
const module1 = (function () {
    // Private data
    let privateData = "private data";
    // Returns object with pulic data and methods
    return {
        publicData: "public data",
        publicMethod: () => {
            return privateData;
        }
    }
})();
// Use module1
console.log(module1.publicData);
console.log(module1.publicMethod());

