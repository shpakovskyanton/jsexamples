'use strict';

/*
* Prototype allows you to create object copies. The object itself to responsible clone.
*/

class Ball {
    /**
     * Ball constructor
     * @param radius
     * @param weight
     */
    constructor(radius, weight) {
        this.radius = radius;
        this.weight = weight;
    }

    /**
     * Returns new instance of object
     */
    clone() {
        return new Ball(this.radius, this.weight);
    }
}

class ColorBall extends Ball {
    /**
     * Color ball constructor
     * @param radius
     * @param weight
     * @param color
     */
    constructor(radius, weight, color) {
        super(radius, weight);
        this.color = color;
    }

    /**
     * Returns new instance of object
     * @returns {ColorBall}
     */
    clone() {
        return new ColorBall(this.radius, this.weight, this.color);
    }

    /**
     * Returns ball's color
     * @returns {*}
     */
    getColor() {
        return this.color;
    }

    /**
     * Returns ball's color
     * @param color
     */
    setColor(color) {
        this.color = color;
    }

}

//Create instances
const ball = new Ball(1, 1);
const ballClone = ball.clone();

const redBall = new ColorBall(1, 1, 'red');
console.log('Red ball: ' + redBall.getColor());

const blueBall = new ColorBall(1, 2, 'blue');
console.log('Blue ball: ' + blueBall.getColor());

const greenBall = new ColorBall(1000, 2, 'green');
console.log('Green ball: ' + greenBall.getColor());

// Create objects clones
const redClone = redBall.clone();
console.log('Red clone: ' + redClone.getColor());

const blueClone = blueBall.clone();
console.log('Blue clone: ' + blueClone.getColor());

const greenClone = greenBall.clone();
console.log('Green clone: ' + greenClone.getColor());

console.log('Red ball and blue clone ball color set to black');
redBall.setColor('black');
blueClone.setColor('black');
// Manipulate with objects
console.log('Red ball: ' + redBall.getColor());
console.log('Blue ball: ' + blueBall.getColor());
console.log('Green ball: ' + greenBall.getColor());
console.log('Red clone: ' + redClone.getColor());
console.log('Blue clone: ' + blueClone.getColor());
console.log('Green clone: ' + greenClone.getColor());