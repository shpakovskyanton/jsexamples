'use strict';

function CPU() {
    this.freeze = function () {
        /* ... */
    };

    this.jump = function (address) {
        /* ... */
    };

    this.execute = function () {
        /* ... */
    };
}

function HDD() {
    this.read = function (address, size) {
        /* ... */
    };

    this.write = function (address, data) {
        /* ... */
    };
}

function RAM() {
    this.read = function (address, size) {
        /* ... */
    };

    this.write = function (address, data) {
        /* ... */
    };
}

/*
    We need to create an object that provides a common interface for working with subsystems.
    The implementation of the subsystems is hidden from the user, and the work with the
    components of the subsystem is carried out through the facade.
    Facade "Computer" provides interface to subsystems and allows us to work with them
    without knowledge of theirs implementation.
 */
function Computer() {
    this.cpu = new CPU();
    this.hdd = new HDD();
    this.ram = new RAM();

    this.start = function (bootAddress, bootSector) {
        this.cpu.freeze();
        this.ram.write(bootAddress, this.hdd.read(bootSector));
        this.cpu.jump(bootAddress);
        this.cpu.execute();
    }
}

const computer = new Computer();
computer.start(0xdeadbeef, 0xdeadbeef);
